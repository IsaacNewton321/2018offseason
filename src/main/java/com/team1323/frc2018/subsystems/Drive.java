package com.team1323.frc2018.subsystems;

import java.util.Arrays;
import java.util.List;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.StatusFrameEnhanced;
import com.ctre.phoenix.motorcontrol.VelocityMeasPeriod;
import com.team1323.frc2018.Constants;
import com.team1323.frc2018.DriveMotionPlanner;
import com.team1323.frc2018.Ports;
import com.team1323.frc2018.RobotState;
import com.team1323.frc2018.loops.ILooper;
import com.team1323.frc2018.loops.Loop;
import com.team254.drivers.LazyTalonSRX;
import com.team254.lib.geometry.Pose2dWithCurvature;
import com.team254.lib.geometry.Rotation2d;
import com.team254.lib.trajectory.TrajectoryIterator;
import com.team254.lib.trajectory.timing.TimedState;
import com.team254.lib.util.DriveSignal;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Drive extends Subsystem{
	private final LazyTalonSRX leftMaster, leftSlave, /*leftSlave2,*/
		rightMaster, rightSlave/*, rightSlave2*/;
	private List<LazyTalonSRX> motors;
	private List<LazyTalonSRX> slaves;
	private Pigeon pigeon;
	private Solenoid shifter;
	private boolean isHighGear = true;
	private boolean isBrakeMode = false;
	public boolean isHighGear(){
		return isHighGear;
	}
	public boolean isBrakeMode() {
        return isBrakeMode;
	}
	private double maxSpeedFactor = 1.0;
	public void setMaxSpeedFactor(double maxSpeed){
		maxSpeedFactor = maxSpeed;
	}
	
	// The robot drivetrain's various states.
    public enum DriveControlState {
        OPEN_LOOP, // open loop voltage control
        PATH_FOLLOWING
    }
    
    // Control states
    private DriveControlState mDriveControlState;
    public DriveControlState currentState(){
    	return mDriveControlState;
    }
    public void setState(DriveControlState state){
    	mDriveControlState = state;
    }
    
	private DriveMotionPlanner mMotionPlanner;
	public boolean hasFinishedPath(){
		return mMotionPlanner.isDone() && currentState() == DriveControlState.PATH_FOLLOWING;
	}
	
	private static Drive instance = null;
	
	public static Drive getInstance(){
		if(instance == null){
			instance = new Drive();
		}
		return instance;
	}
	
	private Drive(){
		leftMaster = new LazyTalonSRX(Ports.DRIVE_LEFT_MASTER);
		leftSlave = new LazyTalonSRX(Ports.DRIVE_LEFT_SLAVE);
		//leftSlave2 = new LazyTalonSRX(Ports.DRIVE_LEFT_SLAVE_2);
		rightMaster = new LazyTalonSRX(Ports.DRIVE_RIGHT_MASTER);
		rightSlave = new LazyTalonSRX(Ports.DRIVE_RIGHT_SLAVE);
		//rightSlave2 = new LazyTalonSRX(Ports.DRIVE_RIGHT_SLAVE_2);

		motors = Arrays.asList(leftMaster, leftSlave, /*leftSlave2,*/
			rightMaster, rightSlave/*, rightSlave2*/);
		slaves = Arrays.asList(leftSlave, /*leftSlave2,*/ rightSlave/*, rightSlave2*/);

		pigeon = Pigeon.getInstance();

		shifter = new Solenoid(20, Ports.DRIVE_SHIFTER);
		
		leftMaster.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 10);
		rightMaster.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 10);
		checkSensors();
		
		leftMaster.configVelocityMeasurementPeriod(VelocityMeasPeriod.Period_50Ms, 10);
        leftMaster.configVelocityMeasurementWindow(1, 10);
        rightMaster.configVelocityMeasurementPeriod(VelocityMeasPeriod.Period_50Ms, 10);
        rightMaster.configVelocityMeasurementWindow(1, 10);
        
        leftMaster.setStatusFramePeriod(StatusFrameEnhanced.Status_2_Feedback0, 5, 100);
        rightMaster.setStatusFramePeriod(StatusFrameEnhanced.Status_2_Feedback0, 5, 100);
				
		leftMaster.setInverted(false);
		rightMaster.setInverted(true);
		leftSlave.setInverted(false);
		rightSlave.setInverted(true);
		//leftSlave2.setInverted(false);
		//rightSlave2.setInverted(false);

		leftMaster.setSensorPhase(true);
		rightMaster.setSensorPhase(true);
		
		for(LazyTalonSRX talon : motors){
			talon.configPeakOutputForward(1.0, 10);
			talon.configPeakOutputReverse(-1.0, 10);
			talon.enableVoltageCompensation(true);
			talon.configVoltageCompSaturation(12.0, 10);
		}
		
		leftMaster.selectProfileSlot(0, 0);
		rightMaster.selectProfileSlot(0, 0);
		reloadGains();
		
		leftMaster.set(ControlMode.PercentOutput, 0);
		leftSlave.set(ControlMode.Follower, Ports.DRIVE_LEFT_MASTER);
		//leftSlave2.set(ControlMode.Follower, Ports.DRIVE_LEFT_MASTER);
		//leftSlave.set(ControlMode.PercentOutput, 0.0);
		//leftSlave2.set(ControlMode.PercentOutput, 0.0);
		//rightSlave.set(ControlMode.PercentOutput, 0.0);
		//rightSlave2.set(ControlMode.PercentOutput, 0.0);
		rightMaster.set(ControlMode.PercentOutput, 0);
		rightSlave.set(ControlMode.Follower, Ports.DRIVE_RIGHT_MASTER);
		//rightSlave2.set(ControlMode.Follower, Ports.DRIVE_RIGHT_MASTER);
		
		mMotionPlanner = new DriveMotionPlanner();
						
		setOpenLoop(DriveSignal.NEUTRAL);
	}
	
	private final Loop loop = new Loop(){
		@Override
		public void onStart( double timestamp){
			setOpenLoop(DriveSignal.NEUTRAL);
		}
		
		@Override
		public void onLoop(double timestamp){
			synchronized (Drive.this){
				switch(mDriveControlState){
				case OPEN_LOOP:
					break;
				case PATH_FOLLOWING:
					updatePathFollower();
					break;
                default:
                    System.out.println("Unexpected drive control state: " + mDriveControlState);
                    break;
				}
			}
		}
		
		@Override
		public void onStop(double timestamp){
			setOpenLoop(DriveSignal.NEUTRAL);
		}
	};
	
	@Override
	public void registerEnabledLoops(ILooper looper){
		looper.register(loop);
	}
	
	protected synchronized void setLeftRightPower(double left, double right){
		leftMaster.set(ControlMode.PercentOutput, left * maxSpeedFactor);
		rightMaster.set(ControlMode.PercentOutput, right * maxSpeedFactor);
		//leftSlave.set(ControlMode.PercentOutput, left);
		//rightSlave.set(ControlMode.PercentOutput, right);
	}
	
	public synchronized void setOpenLoop(DriveSignal signal){
		if(currentState() != DriveControlState.OPEN_LOOP){
			setState(DriveControlState.OPEN_LOOP);
			setBrakeMode(true);
		}
		setLeftRightPower(signal.getLeft(), signal.getRight());
	}
	
	public synchronized void setVelocity(DriveSignal signal){
		if(currentState() != DriveControlState.PATH_FOLLOWING)
			setState(DriveControlState.PATH_FOLLOWING);
		leftMaster.set(ControlMode.Velocity, signal.getLeft());
		rightMaster.set(ControlMode.Velocity, signal.getRight());
	}
	
	public synchronized void setTrajectory(TrajectoryIterator<TimedState<Pose2dWithCurvature>> trajectory) {
        if(mMotionPlanner != null) {
            mMotionPlanner.reset();
            mMotionPlanner.setTrajectory(trajectory);
            mDriveControlState = DriveControlState.PATH_FOLLOWING;
        }
    }

    public synchronized void setBrakeMode(boolean on) {
    	NeutralMode mode = NeutralMode.Coast;
    	if(on) mode = NeutralMode.Brake;
        leftMaster.setNeutralMode(mode);
        leftSlave.setNeutralMode(mode);
        rightMaster.setNeutralMode(mode);
        rightSlave.setNeutralMode(mode);
	}
	
	public synchronized void setHighGear(boolean high){
		if(high && !isHighGear()){
			shifter.set(false);
			isHighGear = true;
		}else if(!high && isHighGear()){
			shifter.set(true);
			isHighGear = false;
		}
	}
    
    public double encUnitsToFeet(double encUnits){
    	return encUnits / Constants.kEncoderResolution * (Math.PI * Constants.kDriveWheelDiameterFeet);
    }
    
    public double feetToEncUnits(double feet){
    	return feet / (Math.PI * Constants.kDriveWheelDiameterFeet) * Constants.kEncoderResolution;
    }
    
    public double encVelocityToFeetPerSecond(double encVelocity){
    	return encUnitsToFeet(encVelocity) * 10.0;
    }
    
    public double feetPerSecondToEncVelocity(double feetPerSecond){
    	return feetToEncUnits(feetPerSecond) / 10.0;
    }
    
    private static double radiansPerSecondToTicksPer100ms(double rad_s) {
        return rad_s /(Math.PI * 2.0) * 4096.0 / 10.0;
    }
    
    public double getLeftDistance(){
    	return encUnitsToFeet(leftMaster.getSelectedSensorPosition(0));
    }
    
    public double getRightDistance(){
    	return encUnitsToFeet(rightMaster.getSelectedSensorPosition(0));
    }
    
    public double getLeftLinearVelocity(){
		return encVelocityToFeetPerSecond(leftMaster.getSelectedSensorVelocity(0));
    }
    
    public double getRightLinearVelocity(){
    	return encVelocityToFeetPerSecond(rightMaster.getSelectedSensorVelocity(0));
	}
	
	public Rotation2d getHeading(){
		return pigeon.getAngle();
	}

	public void setHeading(Rotation2d heading){
		pigeon.setAngle(heading.getDegrees());
	}
    
    private void updatePathFollower() {
        if(mDriveControlState == DriveControlState.PATH_FOLLOWING) {
            final double now = Timer.getFPGATimestamp();

            DriveMotionPlanner.Output output = mMotionPlanner.update(now, RobotState.getInstance().getFieldToVehicle(now));
            
            setVelocity(new DriveSignal(radiansPerSecondToTicksPer100ms(output.left_velocity), radiansPerSecondToTicksPer100ms(output.right_velocity)));
        } else {
            DriverStation.reportError("Drive is not in path following state", false);
        }
    }
	
	@Override
	public synchronized void stop(){
		setOpenLoop(DriveSignal.NEUTRAL);
	}
	
	@Override
	public void outputTelemetry(){
		SmartDashboard.putNumber("Right Master Voltage", rightMaster.getMotorOutputVoltage());
		SmartDashboard.putNumber("Right Slave Voltage", rightSlave.getMotorOutputVoltage());
		SmartDashboard.putNumber("Left Master Voltage", leftMaster.getMotorOutputVoltage());
		SmartDashboard.putNumber("Left Slave Voltage", leftSlave.getMotorOutputVoltage());
		SmartDashboard.putNumber("Right Master Current", rightMaster.getOutputCurrent());
		SmartDashboard.putNumber("Left Master Current", leftMaster.getOutputCurrent());
		SmartDashboard.putNumber("Left Slave Current", leftSlave.getOutputCurrent());
		SmartDashboard.putNumber("Right Slave Current", rightSlave.getOutputCurrent());
		SmartDashboard.putNumber("Left Encoder", getLeftDistance());
		SmartDashboard.putNumber("Right Encoder", getRightDistance());
		SmartDashboard.putNumber("Right Raw Encoder", rightMaster.getSelectedSensorPosition(0));
		SmartDashboard.putNumber("Left Velocity", getLeftLinearVelocity());
		SmartDashboard.putNumber("Right Velocity", getRightLinearVelocity());
	}
	
	@Override
	public synchronized void zeroSensors(){
		leftMaster.setSelectedSensorPosition(0, 0, 10);
		rightMaster.setSelectedSensorPosition(0, 0, 10);
	}
	
	public synchronized void checkSensors(){
		boolean leftSensor = leftMaster.getSensorCollection().getPulseWidthRiseToRiseUs() != 0;
		boolean rightSensor = rightMaster.getSensorCollection().getPulseWidthRiseToRiseUs() != 0;
		
		if(!leftSensor){
			DriverStation.reportError("Left encoder not detected!", false);
		}
		if(!rightSensor){
			DriverStation.reportError("Right encoder not detected!", false);
		}
	}
	
	private void reloadGains(){
		leftMaster.config_kP(0, 1.0, 10);//1.25
		leftMaster.config_kI(0, 0.0, 10);
		leftMaster.config_kD(0, 10.0, 10);
		leftMaster.config_kF(0, 1023.0 / 4500.0, 10);
		
		rightMaster.config_kP(0, 1.0, 10);
		rightMaster.config_kI(0, 0.0, 10);
		rightMaster.config_kD(0, 10.0, 10);
		rightMaster.config_kF(0, 1023.0 / 4500.0, 10);
	}
	
	
}
