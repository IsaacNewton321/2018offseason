package com.team1323.frc2018.subsystems;

import com.team1323.frc2018.Constants;
import com.team1323.frc2018.Kinematics;
import com.team1323.frc2018.RobotState;
import com.team1323.frc2018.loops.ILooper;
import com.team1323.frc2018.loops.Loop;
import com.team254.lib.geometry.Rotation2d;
import com.team254.lib.geometry.Translation2d;
import com.team254.lib.geometry.Twist2d;

public class RobotStateEstimator extends Subsystem {
    static RobotStateEstimator instance_ = new RobotStateEstimator();
    private RobotState robot_state_ = RobotState.getInstance();
    private Drive drive_ = Drive.getInstance();
    private double left_encoder_prev_distance_ = 0.0;
    private double right_encoder_prev_distance_ = 0.0;
    private double back_encoder_prev_distance_ = 0.0;

    RobotStateEstimator() {
    }

    public static RobotStateEstimator getInstance() {
        return instance_;
    }

    @Override
    public void outputTelemetry() {
        // No-op
    }

    @Override
    public void stop() {
        // No-op
    }

    @Override
    public void registerEnabledLoops(ILooper looper) {
        looper.register(new EnabledLoop());
    }

    private class EnabledLoop implements Loop {
        @Override
        public synchronized void onStart(double timestamp) {
            left_encoder_prev_distance_ = drive_.getLeftDistance();
            right_encoder_prev_distance_ = drive_.getRightDistance();

        }

        @Override
        public synchronized void onLoop(double timestamp) {
            final double left_distance = drive_.getLeftDistance();
            final double right_distance = drive_.getRightDistance();
            double delta_left = left_distance - left_encoder_prev_distance_;
            double delta_right = right_distance - right_encoder_prev_distance_;
            final Rotation2d gyro_angle = drive_.getHeading();

            Translation2d leftTranslation = Translation2d.fromPolar(gyro_angle, delta_left);
            Translation2d rightTranslation = Translation2d.fromPolar(gyro_angle, delta_right);
            leftTranslation = new Translation2d(leftTranslation.x() * ((Math.signum(leftTranslation.x()) == -1.0) ? Constants.kXScrubFactor : 1.0),
                leftTranslation.y() * ((Math.signum(leftTranslation.y()) == -1.0) ? Constants.kYScrubFactor : 1.0));
            rightTranslation = new Translation2d(rightTranslation.x() * ((Math.signum(rightTranslation.x()) == -1.0) ? Constants.kXScrubFactor : 1.0),
                rightTranslation.y() * ((Math.signum(rightTranslation.y()) == -1.0) ? Constants.kYScrubFactor : 1.0));
            delta_left = leftTranslation.norm() * Math.signum(delta_left);
            delta_right = rightTranslation.norm() * Math.signum(delta_right);

            final Twist2d odometry_velocity = robot_state_.generateOdometryFromSensors(
                    delta_left, delta_right, gyro_angle);
            final Twist2d predicted_velocity = Kinematics.forwardKinematics(drive_.getLeftLinearVelocity(),
                    drive_.getRightLinearVelocity());
            robot_state_.addObservations(timestamp, odometry_velocity,
                    predicted_velocity);
            left_encoder_prev_distance_ = left_distance;
            right_encoder_prev_distance_ = right_distance;
        }

        @Override
        public void onStop(double timestamp) {
            // no-op
        }
    }
}

