package com.team1323.frc2018.auto.modes;

import java.util.Arrays;
import java.util.List;

import com.team1323.frc2018.Constants;
import com.team1323.frc2018.auto.AutoModeBase;
import com.team1323.frc2018.auto.AutoModeEndedException;
import com.team1323.frc2018.auto.actions.ResetPoseAction;
import com.team1323.frc2018.auto.actions.SetTrajectoryAction;
import com.team1323.frc2018.auto.actions.WaitAction;
import com.team1323.frc2018.auto.actions.WaitForElevatorAction;
import com.team1323.frc2018.auto.actions.WaitToFinishPathAction;
import com.team1323.frc2018.auto.actions.WaitToIntakeCubeAction;
import com.team1323.frc2018.auto.actions.WaitToPassYCoordinateAction;
import com.team1323.frc2018.subsystems.Intake;
import com.team1323.frc2018.subsystems.Intake.IntakeState;
import com.team1323.frc2018.subsystems.RequestList;
import com.team1323.frc2018.subsystems.Superstructure;
import com.team254.frc2018.paths.TrajectoryGenerator;
import com.team254.lib.geometry.Pose2dWithCurvature;
import com.team254.lib.trajectory.Trajectory;
import com.team254.lib.trajectory.timing.TimedState;

import edu.wpi.first.wpilibj.Timer;

public class RightScaleMode extends AutoModeBase{
	Superstructure s;
	Intake intake;
	
    private List<Trajectory<TimedState<Pose2dWithCurvature>>> paths = Arrays.asList(trajectories.sideStartToFarScale,
		trajectories.rightScaleToFirstCube, trajectories.firstCubeToRightScale, trajectories.rightScaleToSecondCube,
		trajectories.secondCubeToRightScale);

	@Override
	public List<Trajectory<TimedState<Pose2dWithCurvature>>> getPaths(){
		return paths;
	}

	public RightScaleMode(){
		s = Superstructure.getInstance();
		intake = Intake.getInstance();
	}

	@Override
	protected void routine() throws AutoModeEndedException {
		double startTime = Timer.getFPGATimestamp();
		runAction(new ResetPoseAction(TrajectoryGenerator.kSideStartPose));
		s.request(intake.stateRequest(IntakeState.CLAMPING));
		runAction(new SetTrajectoryAction(trajectories.sideStartToFarScale));
		runAction(new WaitToPassYCoordinateAction(17.0));
		s.request(s.elevatorWristConfig(Constants.kElevatorHighScaleHeight, Constants.kWristPrimaryStowAngle));
        runAction(new WaitToFinishPathAction());
        runAction(new WaitForElevatorAction());
		s.request(intake.ejectRequest(-1.0));
		System.out.println("First Cube Scored at: " + (Timer.getFPGATimestamp() - startTime));
        runAction(new WaitAction(1.0));
        s.request(s.elevatorWristIntakeConfig(Constants.kElevatorIntakingHeight, Constants.kWristIntakingAngle, IntakeState.OPEN));
        runAction(new SetTrajectoryAction(trajectories.rightScaleToFirstCube));
        runAction(new WaitToFinishPathAction());
        runAction(new WaitForElevatorAction());
        s.request(s.intake.stateRequest(IntakeState.INTAKING));
		runAction(new WaitToIntakeCubeAction(1.5));
		runAction(new SetTrajectoryAction(trajectories.firstCubeToRightScale));
		s.request(s.intake.stateRequest(IntakeState.INTAKING));
		runAction(new WaitAction(0.5));
        s.request(s.elevatorWristIntakeConfig(Constants.kElevatorHighScaleHeight, Constants.kWristPrimaryStowAngle, IntakeState.CLAMPING));
        runAction(new WaitToFinishPathAction());
        runAction(new WaitForElevatorAction());
        s.request(intake.ejectRequest(-1.0));
		System.out.println("Second Cube Scored at: " + (Timer.getFPGATimestamp() - startTime));
		runAction(new WaitAction(1.0));
		s.request(s.elevatorWristIntakeConfig(Constants.kElevatorIntakingHeight, Constants.kWristIntakingAngle, IntakeState.OFF),
			new RequestList(intake.stateRequest(IntakeState.INTAKING)));
        runAction(new SetTrajectoryAction(trajectories.rightScaleToSecondCube));
        runAction(new WaitToFinishPathAction());
        runAction(new WaitForElevatorAction());
        s.request(s.intake.stateRequest(IntakeState.INTAKING));
		runAction(new WaitToIntakeCubeAction(1.5));
		runAction(new SetTrajectoryAction(trajectories.secondCubeToRightScale));
		//s.request(s.intake.stateRequest(IntakeState.INTAKING));
		runAction(new WaitAction(0.5));
        s.request(s.elevatorWristIntakeConfig(Constants.kElevatorHighScaleHeight, Constants.kWristPrimaryStowAngle, IntakeState.CLAMPING));
        runAction(new WaitToFinishPathAction());
        runAction(new WaitForElevatorAction());
        s.request(intake.ejectRequest(-1.0));
		System.out.println("Third Cube Scored at: " + (Timer.getFPGATimestamp() - startTime));
	}
	
}
