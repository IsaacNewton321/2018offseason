package com.team1323.frc2018.auto.actions;

import com.team1323.frc2018.subsystems.Drive;

import edu.wpi.first.wpilibj.Timer;

public class WaitToFinishPathAction implements Action{
	Drive drive;
	double timeout;
	double startTime;
	
	public WaitToFinishPathAction(){
		drive = Drive.getInstance();
		timeout = 15.0;
	}
	
	public WaitToFinishPathAction(double timeout){
		drive = Drive.getInstance();
		this.timeout = timeout;
	}
	
	@Override
	public boolean isFinished(){
		return drive.hasFinishedPath() || ((Timer.getFPGATimestamp() - startTime) > timeout);
	}
	
	@Override
	public void start(){
		startTime = Timer.getFPGATimestamp();
	}
	
	@Override
	public void update(){
	}
	
	@Override
	public void done(){
	}
}
