package com.team1323.frc2018.auto.actions;

import com.team1323.frc2018.RobotState;

public class WaitToPassYCoordinateAction implements Action{
	double startingYCoordinate;
	double targetYCoordinate;
	RobotState robotState;
	
	public WaitToPassYCoordinateAction(double y){
		targetYCoordinate = y;
		robotState = RobotState.getInstance();
	}
	
	@Override
	public boolean isFinished() {
		return Math.signum(startingYCoordinate - targetYCoordinate) !=
				Math.signum(robotState.getLatestFieldToVehicle().getValue().getTranslation().y() - targetYCoordinate);
	}

	@Override
	public void start() {
		startingYCoordinate = robotState.getLatestFieldToVehicle().getValue().getTranslation().y();
	}

	@Override
	public void update() {
		
	}

	@Override
	public void done() {
		
	}
}
