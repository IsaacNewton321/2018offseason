package com.team1323.frc2018.auto.actions;

import com.team1323.frc2018.RobotState;

public class WaitToPassXCoordinateAction implements Action{
	double startingXCoordinate;
	double targetXCoordinate;
	RobotState robotState;
	
	public WaitToPassXCoordinateAction(double x){
		targetXCoordinate = x;
		robotState = RobotState.getInstance();
	}
	
	@Override
	public boolean isFinished() {
		return Math.signum(startingXCoordinate - targetXCoordinate) !=
				Math.signum(robotState.getLatestFieldToVehicle().getValue().getTranslation().x() - targetXCoordinate);
	}

	@Override
	public void start() {
		startingXCoordinate = robotState.getLatestFieldToVehicle().getValue().getTranslation().x();
	}

	@Override
	public void update() {
		
	}

	@Override
	public void done() {
		
	}

}
