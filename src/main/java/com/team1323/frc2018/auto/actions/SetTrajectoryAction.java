package com.team1323.frc2018.auto.actions;

import com.team1323.frc2018.subsystems.Drive;
import com.team254.lib.geometry.Pose2dWithCurvature;
import com.team254.lib.trajectory.TimedView;
import com.team254.lib.trajectory.Trajectory;
import com.team254.lib.trajectory.TrajectoryIterator;
import com.team254.lib.trajectory.timing.TimedState;

public class SetTrajectoryAction extends RunOnceAction{
	Trajectory<TimedState<Pose2dWithCurvature>> trajectory;
	Drive drive;
	
	public SetTrajectoryAction(Trajectory<TimedState<Pose2dWithCurvature>> trajectory){
		this.trajectory = trajectory;
		drive = Drive.getInstance();
	}
	
	@Override
	public synchronized void runOnce(){
		drive.setTrajectory(new TrajectoryIterator<>(new TimedView<>(trajectory)));
	}
}
