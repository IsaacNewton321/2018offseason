package com.team1323.frc2018.auto.actions;

import com.team1323.frc2018.RobotState;
import com.team254.lib.geometry.Pose2d;

import edu.wpi.first.wpilibj.Timer;

public class ResetPoseAction extends RunOnceAction{
	private Pose2d newPose;
	
	public ResetPoseAction(Pose2d newPose){
		this.newPose = newPose;
	}

	@Override
	public void runOnce() {
		RobotState.getInstance().reset(Timer.getFPGATimestamp(), newPose);
	}

}
