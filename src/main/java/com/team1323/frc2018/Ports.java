package com.team1323.frc2018;

public class Ports {
	//Talons
	public static final int DRIVE_LEFT_MASTER = 2;
    public static final int DRIVE_LEFT_SLAVE = 0;
    public static final int DRIVE_LEFT_SLAVE_2 = 1;
	public static final int DRIVE_RIGHT_MASTER = 13;
    public static final int DRIVE_RIGHT_SLAVE = 15;
    public static final int DRIVE_RIGHT_SLAVE_2 = 14;
    

	public static final int ELEVATOR_1 = 3;
    public static final int ELEVATOR_2 = 7;
    public static final int ELEVATOR_3 = 8;
    public static final int ELEVATOR_4 = 12;
    
    public static final int INTAKE_LEFT = 5;
    public static final int INTAKE_RIGHT = 6;
    
    public static final int WRIST = 9;
    
    public static final int PIGEON_TALON = 1;
    
    //Solenoids
    public static final int INTAKE_PINCHERS = 1;
    public static final int INTAKE_CLAMPERS = 0;

    public static final int DRIVE_SHIFTER = 2;
    
    //Digital Inputs 
    public static final int INTAKE_BANNER = 0;
}
