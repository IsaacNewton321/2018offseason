package com.team1323.frc2018;

import com.team1323.lib.util.InterpolatingDouble;
import com.team1323.lib.util.InterpolatingTreeMap;
import com.team254.lib.geometry.Translation2d;;

public class Constants {
	/* All distance measurements are in feet. */
	
	public static final double kLooperDt = 0.01;
	
	public static final double kDriveWheelDiameterFeet = 3.5244 / 12.0;
	public static final double kDriveWheelRadiusFeet = kDriveWheelDiameterFeet / 2.0;
	public static final double kDriveWheelTrackWidthFeet = 25.875 / 12.0;
	public static final double kRobotLengthFeet = 35.25 / 12.0;
	public static final double kRobotHalfLength = kRobotLengthFeet / 2.0;
	public static final double kRobotWidthFeet = 34.25 / 12.0;
	public static final double kRobotHalfWidth = kRobotWidthFeet / 2.0;
	public static final double kEncoderResolution = 4096.0;

	public static final double kXScrubFactor = 1.0 / ((12.86106 - 0.3086) / 12.86106);
	public static final double kYScrubFactor = 1.0 / ((18.41073 - 0.32915) / 18.41073);
	
	public static final double kPathKX = 4.0 / 12.0;  // units/s per unit of error
    public static final double kPathLookaheadTime = 0.4;  // seconds to look ahead along the path for steering
    public static final double kPathMinLookaheadDistance = 24.0 / 12.0;  // inches
	
	// Tuned dynamics
    public static final double kRobotLinearInertia = 60.0;  // kg TODO tune
    public static final double kRobotAngularInertia = 10.0;  // kg m^2 TODO tune
    public static final double kRobotAngularDrag = 12.0;  // N*m / (rad/sec) TODO tune
    public static final double kDriveVIntercept = 1.055;  // V
    public static final double kDriveKv = 0.135;  // V per rad/s
    public static final double kDriveKa = 0.012;  // V per rad/s^2
    
    public static final double kCubeWidth = 13.0/12.0;
    
    public static final Translation2d kAutoStartingCorner = new Translation2d(kRobotLengthFeet / 2.0, 12.5);
    
 	//Field Landmarks
  	public static final Translation2d kRightSwitchCloseCorner = new Translation2d(140.0 / 12.0, 27.0 - (85.25/12.0));
  	public static final Translation2d kRightSwitchFarCorner = new Translation2d(196.0 / 12.0, 27.0 - (85.25/12.0));
  	public static final Translation2d kLeftSwitchCloseCorner = new Translation2d(140.0 / 12.0, 85.25/12.0);
  	public static final Translation2d kLeftSwitchFarCorner = new Translation2d(196.0 / 12.0, 85.25/12.0);
  	public static final Translation2d kRightScaleCorner = new Translation2d(299.65 / 12.0, 27.0 - (95.25/12.0));
  	public static final Translation2d kLeftScaleCorner = new Translation2d(299.65 / 12.0, 95.25 / 12.0);
  	public static final Translation2d kRightMostCube = kRightSwitchFarCorner.translateBy(new Translation2d(kCubeWidth, -0.25));
  	public static final Translation2d kLeftMostCube = kLeftSwitchFarCorner.translateBy(new Translation2d(kCubeWidth, kCubeWidth/2.0));
  	public static final Translation2d kLeftMostCubeCorner = kLeftSwitchFarCorner.translateBy(new Translation2d(kCubeWidth, 0.0));
  	public static final Translation2d kSecondLeftCube = kLeftMostCube.translateBy(new Translation2d(0.0, kCubeWidth + (15.1/12.0)));
	public static final Translation2d kSecondLeftCubeCorner = kSecondLeftCube.translateBy(new Translation2d(0.0, -kCubeWidth/2.0));
	  
	//Elevator Constants
	public static final double kElevatorMaxSpeed =  /*2800.0*/ 1000.0 * 4096.0 / 600.0; //encoder units per 100 ms (4266) 18700 18.06
	/**
	 * Pulse width position of the elevator encoder when it has fully descended.
	 */
	public static final int kElevatorEncoderStartingPosition = 0;
	public static final double kElevatorTicksPerFoot = 13878.0 / 1.901041667; //determined empirically 29.1875 - 6.375 13878
	public static final double kElevatorHeightTolerance = 0.1; //feet
	public static final double kElevatorIntakingHeight = 0.2; //feet
	public static final double kElevatorSecondCubeHeight = 0.97 + 0.1;
	public static final double kElevatorHumanLoadHeight = 1.836 + 0.1;
	public static final double kElevatorSwitchHeight = 2.2; //feet
	public static final double kELevatorBalancedScaleHeight = 5.05; //feet
	public static final double kElevatorHighScaleHeight = 5.3;
	public static final double kElevatorLowScaleHeight = 4.3;
	public static final double kELevatorHangingHeight = 4.9;
	public static final double kElevatorMinHeight = 0.0; //feet
	public static final double kElevatorMaxHeight = 7.9; //feet
	public static final double kElevatorMaxCurrent = 50.0;//amps
	public static final int kELevatorCurrentLimit = 30;
	public static final double kElevatorMinimumHangingHeight = 0.795 + 0.08;
	public static final double kElevatorMaximumHangingHeight = 3.25;
	public static final double kElevatorHangingRampHeight = 3.452;
	public static final double kElevatorTippingCubeHeight = 0.57;
	public static final double kElevatorTeleopManualSpeed = 0.5;

	//Wrist Constants
	public static final double kWristMaxSpeed = /*41.58 * 4096.0 / 600.0*/1000.0; //encoder units per 100 ms
	public static final double kWristStartingAngle = 90.0;
	/**
	 * Pulse width position of the wrist encoder when the wrist is upright (at 90 degrees, parallel to the elevator).
	 */
	public static final int kWristStartingEncoderPosition = 4042;
	/**
	 * The number of rotations the wrist encoder undergoes for every rotation of the wrist.
	 */
	public static final double kWristEncoderToOutputRatio = 26.0 / 12.0;
	public static final double kWristAngleTolerance = 10.0; //degrees
	public static final double kWristMinControlAngle = -2.0; //degrees
	public static final double kWristMaxControlAngle = 182.0; //degrees
	public static final double kWristRangeSplice = 90.0;
	public static final double kWristMinPhysicalAngle = -20.0;
	public static final double kWristMaxPhysicalAngle = 110.0;//95.192
	public static final double kWristIntakingAngle = 179.0;
	public static final double kWristPrimaryStowAngle = 4.0;
	public static final double kWristSecondaryStowAngle = 90.0;
	public static final double kWristHangingAngle = 90.0;
	public static final double kWristMaxStowHeight = 3.5; //height of the elevator
	public static final double kWristMaxCurrent = 40.0;//amps

	//Intake Constants
	public static final double kIntakeWeakEjectOutput = -0.4;
	public static final double kIntakeEjectOutput = -1.0;
	public static final double kIntakeStrongEjectOutput = -1.0;
	public static final double kIntakingOutput = 1.0;
	public static final double kIntakeWeakHoldingOutput = 1.25/12.0;
	public static final double kIntakeStrongHoldingOutput = 4.0/12.0;
	public static final double kIntakingResuckingOutput = 6.0/12.0;
	public static final double kIntakeRampRate = 0.25;
	public static final int kIntakeCurrentLimit = 30;

	//Drive Speed Constraint Treemap
	public static InterpolatingTreeMap<InterpolatingDouble, InterpolatingDouble> kDriveSpeedTreeMap = new InterpolatingTreeMap<>();
	static{
		kDriveSpeedTreeMap.put(new InterpolatingDouble(-0.1), new InterpolatingDouble(1.0));
		kDriveSpeedTreeMap.put(new InterpolatingDouble(0.0), new InterpolatingDouble(1.0));
		kDriveSpeedTreeMap.put(new InterpolatingDouble(kElevatorIntakingHeight), new InterpolatingDouble(1.0));
		kDriveSpeedTreeMap.put(new InterpolatingDouble(kElevatorMaxHeight), new InterpolatingDouble(0.5));
		kDriveSpeedTreeMap.put(new InterpolatingDouble(kElevatorMaxHeight + 0.2), new InterpolatingDouble(0.5));
	}
}
