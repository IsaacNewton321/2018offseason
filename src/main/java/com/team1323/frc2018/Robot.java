/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package com.team1323.frc2018;

import java.util.Arrays;

import com.team1323.frc2018.auto.AutoModeExecuter;
import com.team1323.frc2018.auto.SmartDashboardInteractions;
import com.team1323.frc2018.auto.modes.LeftScaleMode;
import com.team1323.frc2018.loops.Looper;
import com.team1323.frc2018.loops.QuinticPathTransmitter;
import com.team1323.frc2018.subsystems.Drive;
import com.team1323.frc2018.subsystems.Drive.DriveControlState;
import com.team1323.frc2018.subsystems.Elevator;
import com.team1323.frc2018.subsystems.Intake;
import com.team1323.frc2018.subsystems.Intake.IntakeState;
import com.team1323.frc2018.subsystems.RequestList;
import com.team1323.frc2018.subsystems.RobotStateEstimator;
import com.team1323.frc2018.subsystems.SubsystemManager;
import com.team1323.frc2018.subsystems.Superstructure;
import com.team1323.frc2018.subsystems.Wrist;
import com.team1323.io.FlightStick;
import com.team1323.io.SteeringWheel;
import com.team1323.io.Xbox;
import com.team1323.lib.util.Util;
import com.team254.frc2018.paths.TrajectoryGenerator;
import com.team254.lib.geometry.Pose2d;
import com.team254.lib.geometry.Rotation2d;
import com.team254.lib.geometry.Translation2d;
import com.team254.lib.util.CheesyDriveHelper;
import com.team254.lib.util.DriveSignal;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Robot extends IterativeRobot {
	private Drive drive;
	private Elevator elevator;
	private Wrist wrist;
	private Intake intake;
	private Superstructure s;
	private Compressor compressor;
	private SubsystemManager subsystems;
	
	private RobotState robotState = RobotState.getInstance();
	
	private CheesyDriveHelper cheesyDriveHelper = new CheesyDriveHelper();
	
	private Looper enabledLooper = new Looper();
	private Looper disabledLooper = new Looper();
	
	private SmartDashboardInteractions smartDashboardInteractions = new SmartDashboardInteractions();

	private TrajectoryGenerator generator = TrajectoryGenerator.getInstance();
	private QuinticPathTransmitter qTransmitter = QuinticPathTransmitter.getInstance();

	private AutoModeExecuter autoModeExecuter = null;
	
	private FlightStick driverJoystick;
	private SteeringWheel wheel;
	private Xbox coDriver;

	@Override
	public void robotInit() {
		drive = Drive.getInstance();
		elevator = Elevator.getInstance();
		wrist = Wrist.getInstance();
		intake = Intake.getInstance();
		compressor = new Compressor(20);
		s = Superstructure.getInstance();
		subsystems = new SubsystemManager(Arrays.asList(drive, RobotStateEstimator.getInstance(),
			elevator, wrist, intake, s));
		drive.zeroSensors();
		
		wheel = new SteeringWheel(0);
		driverJoystick = new FlightStick(1);
		coDriver = new Xbox(2);
		
		robotState.reset(Timer.getFPGATimestamp(), /*generator.kCenterStartPose*/new Pose2d(new Translation2d(), Rotation2d.fromDegrees(180.0)));
		
		subsystems.registerEnabledLoops(enabledLooper);
		subsystems.registerDisabledLoops(disabledLooper);
		disabledLooper.register(qTransmitter);

		smartDashboardInteractions.initWithDefaults();
		
		generator.generateTrajectories();
		
		qTransmitter.addPaths((new LeftScaleMode()).getPaths());
	}
	
	public void allPeriodic(){
		subsystems.outputToSmartDashboard();
		robotState.outputToSmartDashboard();
	}
	
	@Override
	public void autonomousInit() {
		if(autoModeExecuter != null)
				autoModeExecuter.stop();

		disabledLooper.stop();
		enabledLooper.start();

		compressor.setClosedLoopControl(false);

		String gameData = DriverStation.getInstance().getGameSpecificMessage();
		autoModeExecuter = new AutoModeExecuter();
		autoModeExecuter.setAutoMode(smartDashboardInteractions.getSelectedAutoMode(gameData.substring(0, 2)));
		//autoModeExecuter.setAutoMode(new StandStillMode());
		autoModeExecuter.start();
		SmartDashboard.putString("Game Data", gameData);
		System.out.println(gameData);
	}

	@Override
	public void autonomousPeriodic() {
		allPeriodic();
	}
	
	@Override
	public void teleopInit(){
		disabledLooper.stop();
		enabledLooper.start();

		compressor.setClosedLoopControl(false);

		drive.setHighGear(true);
	}
	
	@Override
	public void teleopPeriodic() {
		wheel.update();
		driverJoystick.update();
		coDriver.update();

		if(coDriver.backButton.wasPressed()){
			s.request(intake.stateRequest(IntakeState.OFF));
		}

		s.sendManualInput(coDriver.getY(Hand.kRight), -coDriver.getY(Hand.kLeft));

		if(coDriver.aButton.wasPressed()){
			s.request(s.elevatorWristConfig(Constants.kElevatorIntakingHeight, 
					Constants.kWristIntakingAngle));
			s.replaceQueue(Arrays.asList(new RequestList(intake.waitForCubeRequest()),
					new RequestList(intake.stateRequest(IntakeState.CLAMPING))));
		}else if(coDriver.aButton.longPressed()){
			if(intake.getState() == IntakeState.INTAKING)
				s.replaceQueue(s.elevatorWristIntakeConfig(0.31, 167.0, IntakeState.CLAMPING));
			else
				s.request(s.elevatorWristIntakeConfig(0.31, 167.0, IntakeState.CLAMPING));
		}else if(coDriver.xButton.wasPressed()){
			s.request(s.elevatorWristIntakeConfig(Constants.kElevatorSwitchHeight, 
					Constants.kWristIntakingAngle, IntakeState.CLAMPING));
		}else if(coDriver.xButton.longPressed()){
			s.request(s.elevatorWristIntakeConfig(Constants.kElevatorSwitchHeight, 
					Constants.kWristIntakingAngle, IntakeState.OPEN));
		}else if(coDriver.bButton.wasPressed()){
			s.request(s.wristIntakeConfig(Constants.kWristPrimaryStowAngle, IntakeState.CLAMPING));
		}else if(coDriver.bButton.longPressed()){
			s.request(s.elevatorWristIntakeConfig(Constants.kElevatorIntakingHeight, 
					Constants.kWristPrimaryStowAngle, IntakeState.OFF));
		}else if(coDriver.yButton.wasPressed()){
			s.request(s.elevatorWristIntakeConfig(Constants.kELevatorBalancedScaleHeight, 
					160.0, IntakeState.CLAMPING));
		}else if(coDriver.POV0.wasPressed()){
			if(Util.epsilonEquals(elevator.getHeight(), Constants.kElevatorSecondCubeHeight, Constants.kElevatorHeightTolerance)){
				s.request(s.elevatorWristIntakeConfig(Constants.kElevatorSwitchHeight, 
					Constants.kWristIntakingAngle, IntakeState.OPEN));
			}else{
				s.request(s.elevatorWristConfig(Constants.kElevatorSecondCubeHeight, 
					Constants.kWristIntakingAngle),
					new RequestList(intake.stateRequest(IntakeState.INTAKING)));
			}

		}else if(coDriver.POV180.wasPressed()){
			/*s.request(s.elevatorWristIntakeConfig(Constants.kElevatorLowScaleHeight, 
					155.0, IntakeState.CLAMPING));*/
		}else if(coDriver.POV90.wasPressed()){
			/*s.request(s.elevatorWristIntakeConfig(Constants.kElevatorTippingCubeHeight,
					Constants.kWristIntakingAngle, IntakeState.OFF));*/
		}else if(coDriver.rightCenterClick.wasPressed()){
			/*s.request(s.elevatorWristConfig(Constants.kElevatorSecondCubeHeight, 
					Constants.kWristIntakingAngle),
					new RequestList(intake.stateRequest(IntakeState.INTAKING)));*/
		}else if(coDriver.leftBumper.wasPressed()){
			
		}else if(coDriver.yButton.longPressed()){
			/*s.request(s.elevatorWristConfig(Constants.kElevatorMaxHeight, 
				Constants.kWristPrimaryStowAngle));*/
		}

		if(coDriver.rightBumper.isBeingPressed()){
			s.request(intake.stateRequest(IntakeState.FORCED_INTAKE));
		}else if(intake.getState() == IntakeState.FORCED_INTAKE){
			s.request(intake.stateRequest(IntakeState.OFF));
		}else if(coDriver.leftTrigger.wasPressed()){
			s.request(intake.stateRequest(IntakeState.OPEN));
		}else if(coDriver.rightTrigger.wasPressed() || coDriver.leftBumper.wasPressed()){
			s.request(intake.ejectRequest(Constants.kIntakeEjectOutput));
		}else if(coDriver.rightTrigger.longPressed()){
			s.request(intake.ejectRequest(Constants.kIntakeWeakEjectOutput));
		}
		

		if(Math.abs(driverJoystick.getYAxis()) != 0){
			drive.setOpenLoop(cheesyDriveHelper.cheesyDrive(-driverJoystick.getYAxis(), 
					wheel.getWheelTurn(), wheel.leftBumper.isBeingPressed(), true));
		}else if(drive.currentState() == DriveControlState.OPEN_LOOP){
			drive.setOpenLoop(cheesyDriveHelper.cheesyDrive(-driverJoystick.getYAxis(), 
					wheel.getWheelTurn(), wheel.leftBumper.isBeingPressed(), true));
		}

		if(driverJoystick.triggerButton.wasPressed()){
			drive.setHighGear(!drive.isHighGear());
		}
		
		if(wheel.yButton.wasPressed()){
			drive.setVelocity(new DriveSignal(drive.feetPerSecondToEncVelocity(6.0), drive.feetPerSecondToEncVelocity(6.0)));
			//robotState.reset(Timer.getFPGATimestamp(), generator.kSideStartPose);
			//drive.setTrajectory(new TrajectoryIterator<>(new TimedView<>(generator.getTrajectorySet().sideStartToNearScale)));
		}/*else if(wheel.xButton.wasPressed()){
			robotState.reset(Timer.getFPGATimestamp(), generator.kSideStartPose);
			drive.setTrajectory(new TrajectoryIterator<>(new TimedView<>(generator.getTrajectorySet().sideStartToFarScale)));
		}else if(wheel.bButton.wasPressed()){
			robotState.reset(Timer.getFPGATimestamp(), generator.kCenterStartPose);
			drive.setTrajectory(new TrajectoryIterator<>(new TimedView<>(generator.getTrajectorySet().centerStartToLeftSwitch)));
		}else if(wheel.aButton.wasPressed()){
			drive.setTrajectory(new TrajectoryIterator<>(new TimedView<>(generator.getTrajectorySet().leftSwitchToCenterStart)));
		}*/

		if(intake.needsToNotifyDrivers()){
			coDriver.rumble(1.0, 1.0);
		}
		
		allPeriodic();
	}
	
	@Override
	public void disabledInit(){
		if(autoModeExecuter != null)
			autoModeExecuter.stop();

		enabledLooper.stop();
		disabledLooper.start();

		subsystems.stop();
	}
	
	@Override
	public void disabledPeriodic(){
		allPeriodic();
	}

	@Override
	public void testInit(){
		elevator.checkSystem();
	}

}
