package com.team254.frc2018.paths;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.team1323.frc2018.Constants;
import com.team1323.frc2018.DriveMotionPlanner;
import com.team254.lib.geometry.Pose2d;
import com.team254.lib.geometry.Pose2dWithCurvature;
import com.team254.lib.geometry.Rotation2d;
import com.team254.lib.geometry.Translation2d;
import com.team254.lib.trajectory.Trajectory;
import com.team254.lib.trajectory.TrajectoryUtil;
import com.team254.lib.trajectory.timing.TimedState;
import com.team254.lib.trajectory.timing.TimingConstraint;

import edu.wpi.first.wpilibj.Timer;

public class TrajectoryGenerator {
    private static final double kMaxVelocity = 12.5;
    private static final double kMaxAccel = 12.5;
    private static final double kMaxCentripetalAccelElevatorDown = 110.0 / 12.0;
    private static final double kMaxCentripetalAccel = 100.0 / 12.0;
    private static final double kMaxVoltage = 9.0;
    private static final double kFirstPathMaxVoltage = 9.0;
    private static final double kFirstPathMaxAccel = 120.0 / 12.0;
    private static final double kFirstPathMaxVel = 120.0 / 12.0;

    private static final double kSimpleSwitchMaxAccel = 100.0 / 12.0;
    private static final double kSimpleSwitchMaxCentripetalAccel = 80.0 / 12.0;
    private static final double kSimpleSwitchMaxVelocity = 120.0 / 12.0;

    private static TrajectoryGenerator mInstance = new TrajectoryGenerator();
    private final DriveMotionPlanner mMotionPlanner;
    private TrajectorySet mTrajectorySet = null;

    public static TrajectoryGenerator getInstance() {
        return mInstance;
    }

    private TrajectoryGenerator() {
        mMotionPlanner = new DriveMotionPlanner();
    }

    public void generateTrajectories() {
        if(mTrajectorySet == null) {
        	double startTime = Timer.getFPGATimestamp();
            System.out.println("Generating trajectories...");
            mTrajectorySet = new TrajectorySet();
            System.out.println("Finished trajectory generation in: " + (Timer.getFPGATimestamp() - startTime) + " seconds");
        }
    }

    public TrajectorySet getTrajectorySet() {
        return mTrajectorySet;
    }

    public Trajectory<TimedState<Pose2dWithCurvature>> generateTrajectory(
            boolean reversed,
            final List<Pose2d> waypoints,
            final List<TimingConstraint<Pose2dWithCurvature>> constraints,
            double max_vel,  // inches/s
            double max_accel,  // inches/s^2
            double max_voltage) {
        return mMotionPlanner.generateTrajectory(reversed, waypoints, constraints, max_vel, max_accel, max_voltage);
    }

    public Trajectory<TimedState<Pose2dWithCurvature>> generateTrajectory(
            boolean reversed,
            final List<Pose2d> waypoints,
            final List<TimingConstraint<Pose2dWithCurvature>> constraints,
            double start_vel,  // inches/s
            double end_vel,  // inches/s
            double max_vel,  // inches/s
            double max_accel,  // inches/s^2
            double max_voltage) {
        return mMotionPlanner.generateTrajectory(reversed, waypoints, constraints, start_vel, end_vel, max_vel, max_accel, max_voltage);
    }

    // CRITICAL POSES
    // Origin is the center of the robot when the robot is placed against the middle of the alliance station wall.
    // +x is towards the center of the field.
    // +y is to the left.
    // ALL POSES DEFINED FOR THE CASE THAT ROBOT STARTS ON RIGHT! (mirrored about +x axis for LEFT)
    public static final Pose2d kSideStartPose = new Pose2d(new Translation2d(Constants.kRobotHalfLength, 5.5 - Constants.kRobotHalfWidth),
        Rotation2d.fromDegrees(180.0));
    public static final Pose2d kCenterStartPose = Pose2d.fromTranslation(new Translation2d(Constants.kRobotHalfLength, 12.5 + Constants.kRobotHalfWidth));
    
    public static final Pose2d kLeftSwitchDeliveryPose = new Pose2d(new Translation2d(Constants.kLeftSwitchCloseCorner.x() - Constants.kRobotHalfLength, Constants.kLeftSwitchCloseCorner.y() + Constants.kRobotHalfWidth),
            Rotation2d.fromDegrees(0.0));
            
    public static final Pose2d kRightSwitchDeliveryPose = new Pose2d(new Translation2d(Constants.kRightSwitchCloseCorner.x() - Constants.kRobotHalfLength, Constants.kRightSwitchCloseCorner.y() - Constants.kRobotHalfWidth),
            Rotation2d.fromDegrees(0.0));
            
    public static final Pose2d kNearScaleEmptyPose = new Pose2d(new Translation2d(23.0, Constants.kLeftScaleCorner.y() - Constants.kRobotHalfWidth),
        Rotation2d.fromDegrees(180.0 + 20.0));//19.4046, 7.29, 160
    public static final Pose2d kFirstLeftCubePose = new Pose2d(new Translation2d(18.9, 7.0),
        Rotation2d.fromDegrees(160.0));//18.74, 8.80, 110
    public static final Pose2d kSecondLeftCubePose = new Pose2d(new Translation2d(18.0, 10.0),
        Rotation2d.fromDegrees(110.0));
    public static final Pose2d kThirdLeftCubePose = new Pose2d(new Translation2d(17.75, 13.0),
        Rotation2d.fromDegrees(110.0));
    
    public static final Pose2d kFarScaleEmptyPose = new Pose2d(new Translation2d(Constants.kRightScaleCorner.x() - Constants.kRobotHalfLength - 0.15, Constants.kRightScaleCorner.y() + Constants.kRobotHalfWidth - 0.25),
        Rotation2d.fromDegrees(180.0 - 10.0));
    public static final Pose2d kFirstRightCubePose = new Pose2d(new Translation2d(19.2, 20.0),
        Rotation2d.fromDegrees(180.0 + 10.0));//19.2456, 18.4846, -114.5
    public static final Pose2d kSecondRightCubePose = new Pose2d(new Translation2d(18.5, 17.0),
        Rotation2d.fromDegrees(-114.5));

    public class TrajectorySet {
        public class MirroredTrajectory {
            public MirroredTrajectory(Trajectory<TimedState<Pose2dWithCurvature>> right) {
                this.right = right;
                this.left = TrajectoryUtil.mirrorTimed(right);
            }

            public Trajectory<TimedState<Pose2dWithCurvature>> get(boolean left) {
                return left ? this.left : this.right;
            }

            public final Trajectory<TimedState<Pose2dWithCurvature>> left;
            public final Trajectory<TimedState<Pose2dWithCurvature>> right;
        }

        public final Trajectory<TimedState<Pose2dWithCurvature>> sideStartToNearScale;
        public final Trajectory<TimedState<Pose2dWithCurvature>> leftScaleToFirstCube;
        public final Trajectory<TimedState<Pose2dWithCurvature>> firstCubeToLeftScale;
        public final Trajectory<TimedState<Pose2dWithCurvature>> leftScaleToSecondCube;
        public final Trajectory<TimedState<Pose2dWithCurvature>> secondCubeToLeftScale;
        public final Trajectory<TimedState<Pose2dWithCurvature>> leftScaleToThirdCube;
        public final Trajectory<TimedState<Pose2dWithCurvature>> thirdCubeToLeftScale;

        public final Trajectory<TimedState<Pose2dWithCurvature>> sideStartToFarScale;
        public final Trajectory<TimedState<Pose2dWithCurvature>> rightScaleToFirstCube;
        public final Trajectory<TimedState<Pose2dWithCurvature>> firstCubeToRightScale;
        public final Trajectory<TimedState<Pose2dWithCurvature>> rightScaleToSecondCube;
        public final Trajectory<TimedState<Pose2dWithCurvature>> secondCubeToRightScale;
        
        public final Trajectory<TimedState<Pose2dWithCurvature>> centerStartToLeftSwitch;
        public final Trajectory<TimedState<Pose2dWithCurvature>> centerStartToRightSwitch;

        public final Trajectory<TimedState<Pose2dWithCurvature>> leftSwitchToCenterStart;

        private TrajectorySet() {
            sideStartToNearScale = getSideStartToNearScale();
            leftScaleToFirstCube = getLeftScaleToFirstCube();
            firstCubeToLeftScale = getFirstCubeToLeftScale();
            leftScaleToSecondCube = getLeftScaleToSecondCube();
            secondCubeToLeftScale = getSecondCubeToLeftScale();
            leftScaleToThirdCube = getLeftScaleToThirdCube();
            thirdCubeToLeftScale = getThirdCubeToLeftScale();

            sideStartToFarScale = getSideStartToFarScale();
            rightScaleToFirstCube = getRightScaleToFirstCube();
            firstCubeToRightScale = getFirstCubeToRightScale();
            rightScaleToSecondCube = getRightScaleToSecondCube();
            secondCubeToRightScale = getSecondCubeToRightScale();

            centerStartToLeftSwitch = getCenterToLeftSwitch();
            centerStartToRightSwitch = getCenterToRightSwitch();
            leftSwitchToCenterStart = getLeftSwitchToCenter();
        }

        private Trajectory<TimedState<Pose2dWithCurvature>> getSideStartToNearScale() {
            List<Pose2d> waypoints = new ArrayList<>();
            waypoints.add(kSideStartPose);
            waypoints.add(new Pose2d(new Translation2d(10.0, kSideStartPose.getTranslation().y()),
            		Rotation2d.fromDegrees(180.0)));
            waypoints.add(kNearScaleEmptyPose);
            return generateTrajectory(true, waypoints, Arrays.asList(),
                    kFirstPathMaxVel, kFirstPathMaxAccel, kFirstPathMaxVoltage);
        }

        private Trajectory<TimedState<Pose2dWithCurvature>> getLeftScaleToFirstCube() {
        	List<Pose2d> waypoints = new ArrayList<>();
            waypoints.add(kNearScaleEmptyPose);
            waypoints.add(kFirstLeftCubePose);
            return generateTrajectory(false, waypoints, Arrays.asList(),
            		kFirstPathMaxVel, kFirstPathMaxAccel, kFirstPathMaxVoltage);
        }

        private Trajectory<TimedState<Pose2dWithCurvature>> getFirstCubeToLeftScale() {
        	List<Pose2d> waypoints = new ArrayList<>();
            waypoints.add(kFirstLeftCubePose);
            waypoints.add(kNearScaleEmptyPose);
            return generateTrajectory(true, waypoints, Arrays.asList(),
            		kFirstPathMaxVel, kFirstPathMaxAccel, kFirstPathMaxVoltage);
        }

        private Trajectory<TimedState<Pose2dWithCurvature>> getLeftScaleToSecondCube() {
        	List<Pose2d> waypoints = new ArrayList<>();
            waypoints.add(kNearScaleEmptyPose);
            waypoints.add(kSecondLeftCubePose);
            return generateTrajectory(false, waypoints, Arrays.asList(),
            		kFirstPathMaxVel, kFirstPathMaxAccel, kFirstPathMaxVoltage);
        }

        private Trajectory<TimedState<Pose2dWithCurvature>> getSecondCubeToLeftScale() {
        	List<Pose2d> waypoints = new ArrayList<>();
            waypoints.add(kSecondLeftCubePose);
            waypoints.add(kNearScaleEmptyPose);
            return generateTrajectory(true, waypoints, Arrays.asList(),
            		kFirstPathMaxVel, 5.0, kFirstPathMaxVoltage);
        }

        private Trajectory<TimedState<Pose2dWithCurvature>> getLeftScaleToThirdCube() {
        	List<Pose2d> waypoints = new ArrayList<>();
            waypoints.add(kNearScaleEmptyPose);
            waypoints.add(kThirdLeftCubePose);
            return generateTrajectory(false, waypoints, Arrays.asList(),
            		kFirstPathMaxVel, 5.0, kFirstPathMaxVoltage);
        }

        private Trajectory<TimedState<Pose2dWithCurvature>> getThirdCubeToLeftScale() {
        	List<Pose2d> waypoints = new ArrayList<>();
            waypoints.add(kThirdLeftCubePose);
            waypoints.add(kNearScaleEmptyPose);
            return generateTrajectory(true, waypoints, Arrays.asList(),
            		kFirstPathMaxVel, 5.0, kFirstPathMaxVoltage);
        }

        private Trajectory<TimedState<Pose2dWithCurvature>> getSideStartToFarScale() {
        	List<Pose2d> waypoints = new ArrayList<>();
            waypoints.add(kSideStartPose);
            waypoints.add(kSideStartPose.transformBy(new Pose2d(new Translation2d(-160.0 / 12.0, 0.0), Rotation2d
                    .fromDegrees(0.0))));
            waypoints.add(kSideStartPose.transformBy(new Pose2d(new Translation2d(-220.0 / 12.0, -60.0 / 12.0), Rotation2d
                    .fromDegrees(90.0))));
            waypoints.add(kSideStartPose.transformBy(new Pose2d(new Translation2d(-220.0 / 12.0, -166.0 / 12.0), Rotation2d
                    .fromDegrees(90.0))));
            waypoints.add(kFarScaleEmptyPose);
            return generateTrajectory(true, waypoints, Arrays.asList(),
            		kFirstPathMaxVel, 8.0, kFirstPathMaxVoltage);
        }//19.88, 19.57 -170.0

        private Trajectory<TimedState<Pose2dWithCurvature>> getRightScaleToFirstCube() {
        	List<Pose2d> waypoints = new ArrayList<>();
            waypoints.add(kFarScaleEmptyPose);
            waypoints.add(kFirstRightCubePose);
            return generateTrajectory(false, waypoints, Arrays.asList(),
            		kFirstPathMaxVel, 8.0, kFirstPathMaxVoltage);
        }

        private Trajectory<TimedState<Pose2dWithCurvature>> getFirstCubeToRightScale(){
            List<Pose2d> waypoints = new ArrayList<>();
            waypoints.add(kFirstRightCubePose);
            waypoints.add(kFarScaleEmptyPose);
            return generateTrajectory(true, waypoints, Arrays.asList(),
            		kFirstPathMaxVel, 8.0, kFirstPathMaxVoltage);
        }

        private Trajectory<TimedState<Pose2dWithCurvature>> getRightScaleToSecondCube(){
            List<Pose2d> waypoints = new ArrayList<>();
            waypoints.add(kFarScaleEmptyPose);
            waypoints.add(kSecondRightCubePose);
            return generateTrajectory(false, waypoints, Arrays.asList(),
            		kFirstPathMaxVel, 8.0, kFirstPathMaxVoltage);
        }

        private Trajectory<TimedState<Pose2dWithCurvature>> getSecondCubeToRightScale(){
            List<Pose2d> waypoints = new ArrayList<>();
            waypoints.add(kSecondRightCubePose);
            waypoints.add(kFarScaleEmptyPose);
            return generateTrajectory(true, waypoints, Arrays.asList(),
            		kFirstPathMaxVel, 8.0, kFirstPathMaxVoltage);
        }
        
        private Trajectory<TimedState<Pose2dWithCurvature>> getCenterToLeftSwitch(){
        	List<Pose2d> waypoints = new ArrayList<>();
            waypoints.add(kCenterStartPose);
            waypoints.add(kLeftSwitchDeliveryPose);
            return generateTrajectory(false, waypoints, Arrays.asList(),
                    kFirstPathMaxVel, kFirstPathMaxAccel, kFirstPathMaxVoltage);
        }

        private Trajectory<TimedState<Pose2dWithCurvature>> getCenterToRightSwitch(){
        	List<Pose2d> waypoints = new ArrayList<>();
            waypoints.add(kCenterStartPose);
            waypoints.add(kRightSwitchDeliveryPose);
            return generateTrajectory(false, waypoints, Arrays.asList(),
                    kFirstPathMaxVel, kFirstPathMaxAccel, kFirstPathMaxVoltage);
        }
        
        private Trajectory<TimedState<Pose2dWithCurvature>> getLeftSwitchToCenter(){
        	List<Pose2d> waypoints = new ArrayList<>();
            waypoints.add(kLeftSwitchDeliveryPose);
            waypoints.add(kCenterStartPose);
            return generateTrajectory(true, waypoints, Arrays.asList(),
                    kFirstPathMaxVel, kFirstPathMaxAccel, kFirstPathMaxVoltage);
        }


    }
}
